/*
 * sigfox.c
 *
 *  Created on: 1 de ago de 2019
 *      Author: oscar
 */


#include <sigfox.h>



//******************** SIGFOX FUNCTIONS ************************************
void fn_init_uart()
{
        char ok[4];
        HAL_UART_Transmit(&huart5,(uint8_t*)"AT$P=0\r\n",8,100);
        HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
        HAL_Delay(100);
        HAL_UART_Transmit(&huart5,(uint8_t*)"AT$DR=905200000\r\n",17,100);
        HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
        HAL_Delay(100);
        HAL_UART_Transmit(&huart5,(uint8_t*)"AT$IF=902200000\r\n",17,100);
        HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
        HAL_Delay(100);
        HAL_UART_Transmit(&huart5,(uint8_t*)"AT$WR\r\n",7,100);
        HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
        HAL_Delay(100);
        HAL_UART_Transmit(&huart5,(uint8_t*)"AT$RC\r\n",7,100);
        HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
        HAL_Delay(100);


		#ifdef DONGLE_KEY
				//public key or private key
				HAL_UART_Transmit(&huart5,(uint8_t*)"ATS410=1\r\n",11,100); //ATS410=1 private key ; ATS410=0 public key
				//HAL_UART_Receive(&huart5,(uint8_t*)ok,4,10);
				 HAL_Delay(500);
		#endif
}

void fn_ID_UART()
{

    //clear_char(buffer_uart);
    //clear_char(id_uart);

    char command[9]="AT$I=10\r\n";


    HAL_UART_Transmit_IT(&huart5,(uint8_t*)command,9);
    HAL_Delay(500);
    //HAL_UART_Receive_IT(&huart5,(uint8_t*)id_uart,strlen(id_uart));
    HAL_UART_Receive_IT(&huart5,(uint8_t*)id_uart,10);
    HAL_Delay(500);
    HAL_UART_Transmit(&huart5,(uint8_t*)command,9,100);
    //HAL_UART_Receive_IT(&huart5,(uint8_t*)id_uart,strlen(id_uart));
    HAL_UART_Receive(&huart5,(uint8_t*)id_uart,10,500);
      hexDec(id_uart);
      //id_sigfox_int = (int) decimal;
    //HAL_Delay(50);
    //RemoveSpaces(buffer_uart);
    //strcpy(id_uart,buffer_uart);


}

void fn_PAC_UART()
{
    //clear_char(buffer_uart);
    //clear_char(pac_uart);

    char command[10]="AT$I=11\r\n";
    HAL_UART_Transmit_IT(&huart5,(uint8_t*)command,strlen(command));
    //HAL_UART_Receive_IT(&huart5,(uint8_t*)id_uart,strlen(id_uart));
    HAL_UART_Receive_IT(&huart5,(uint8_t*)pac_uart,18);
     hexDec(pac_uart);
    // pac_sigfox_int = (int) decimal;
    //HAL_UART_Transmit(&huart5,(uint8_t*)command,10,200);
    //HAL_Delay(10);
    //HAL_UART_Receive(&huart5,(uint8_t*)buffer_uart,buffer_size,1000);

    //HAL_Delay(50);
    //RemoveSpaces(buffer_uart);
    //bstrcpy(pac_uart,buffer_uart);
}

void fn_at_uart()
{
    char command[4]="AT\r\n";

    HAL_UART_Transmit_IT(&huart5,(uint8_t*)command,4);
      HAL_UART_Receive_IT(&huart5,(uint8_t*)at_uart,4);
}

void fn_uart_sleep()
{

 HAL_UART_Transmit_IT(&huart5,(uint8_t*)"AT$P=2\r\n",8);

}

void fn_reset_uart()
{

    HAL_GPIO_WritePin(WS_RST_GPIO_Port,WS_RST_Pin,0);
    HAL_Delay(50);
    HAL_GPIO_WritePin(WS_RST_GPIO_Port,WS_RST_Pin,1);
    HAL_GPIO_DeInit(WS_RST_GPIO_Port,WS_RST_Pin);

}

void fn_wakeup_uart()
{
    HAL_GPIO_WritePin(WS_WAKEUP_GPIO_Port,WS_WAKEUP_Pin,1);
    HAL_Delay(50);
    HAL_GPIO_WritePin(WS_WAKEUP_GPIO_Port,WS_WAKEUP_Pin,0);
    HAL_Delay(50);
    HAL_GPIO_WritePin(WS_WAKEUP_GPIO_Port,WS_WAKEUP_Pin,1);
}

void fn_temp_UART()
{
/*  int tempo = 5;
    if (tmp_value!=0 || tempo!=0)
    {*/
    char temp_uart[7];
    char command[8]="AT$T?\r\n";
    HAL_UART_Transmit_IT(&huart5,(uint8_t*)command,8);
    HAL_Delay(10);
    HAL_UART_Receive_IT(&huart5,(uint8_t*)temp_uart,7);
    HAL_Delay(50);
    uart_temp = atof (temp_uart);
/*  tempo--;
    }
        if(tmp_value>100)
            tmp_value/=10;
        else
        {
            if (tmp_value < 10 || tmp_value > 40)
                tmp_value = 26;
        }*/
    HAL_Delay(50);
}

void fn_volts_UART()
{
/*  int tempo = 5;
    if (tmp_value!=0 || tempo!=0)
    {*/
    char volts_uart[10];
    char command[8]="AT$V?\r\n";
    HAL_UART_Transmit(&huart5,(uint8_t*)command,8,100);
    HAL_UART_Receive(&huart5,(uint8_t*)volts_uart,10,100);
    HAL_Delay(50);


    uart_volt = atof (volts_uart);
}


//#################### PAYLOAD FUNCTIONS #####################################

void fn_send_report_frame()
{
	hea_value = 2;
	fn_reset_uart();
    fn_fprint("SEND REPORT FRAME");
    fn_init_uart();
    char payload_completo2[31]="AT$SF=";
    char ok[4]={0};
    char payload_values2[22]={0};
    fn_mount_values(payload_values2);
    int size_pl = strlen(payload_values2);
    //strcat(payload_completo2,"AT$SF=");
    strcat(payload_completo2,payload_values2);
    //strcat(payload_completo2,"\r\n");
    if(size_pl==22)
        payload_completo2[27] = 48;
    payload_completo2[28] = '\r';
    payload_completo2[29] = '\n';
    payload_completo2[30] = 0;
    HAL_UART_Transmit(&huart5,(uint8_t*)payload_completo2,31,200);
    HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
    fn_uart_sleep();
    fn_fprint(payload_completo2);
}

void fn_send_alert_frame()
{
	hea_value=4;
	fn_reset_uart();
	    fn_fprint("SEND REPORT FRAME");
	    fn_init_uart();
	    char payload_completo2[31]="AT$SF=";
	    char ok[4]={0};
	    char payload_values2[22]={0};
	    fn_mount_values(payload_values2);
	    int size_pl = strlen(payload_values2);
	    //strcat(payload_completo2,"AT$SF=");
	    strcat(payload_completo2,payload_values2);
	    //strcat(payload_completo2,"\r\n");
	    if(size_pl==22)
	        payload_completo2[27] = 48;
	    payload_completo2[28] = '\r';
	    payload_completo2[29] = '\n';
	    payload_completo2[30] = 0;
	    HAL_UART_Transmit(&huart5,(uint8_t*)payload_completo2,31,200);
	    HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
	    fn_uart_sleep();
	    fn_fprint(payload_completo2);
}

void fn_send_daily_frame()
{
	fn_reset_uart();
	fn_init_uart();
	    char payload_completo[33]={0};
	    char config[32]={0};
	    char buff_downlink[16]={0};
	    char buff_dec[64]={0};
	    char volume_ref [3]={0};
	    char timebyte   [6]={0};
	    char undTimeByte[2]={0};
	    char time_stamp [7]={0};
	    int  timebyte_int, undTimeByte_int,time_byte_value=0;
	    char payload_values[22]={0};
	    int d_time=0;
		hea_value=2;
	    //unsigned int try=0;
	    HAL_Delay(3000);
	    for (int var = 0; var < 3; var++)
	    {
	        fn_fprint("SIGFOX DOWNLINK REQUEST");
	        fn_mount_values(payload_values); //payload mount values
	        strcpy(payload_completo,"AT$SF=");
	        strcat(payload_completo,payload_values);
	        payload_completo[28] = 44;
	        payload_completo[29] = 49;
	        payload_completo[30] = 13;
	        payload_completo[31] = 10;
	        payload_completo[32] = 0;
	        //strcat(payload_completo,",1\r\n\0");
	    	HAL_UART_Transmit_IT(&huart5,(uint8_t*)payload_completo,33);
	    	d_time=0;
	        while (UartReady != SET && d_time !=60)
	         {
	        	HAL_Delay(1000);
	        	d_time++;
	         }
	        /* Reset transmission flag */
	        UartReady = RESET;
	        fn_fprint(payload_completo);
	        HAL_UART_Receive_IT(&huart5, (uint8_t *)config,32);
	        	        //HAL_Delay(30000);
	        	    	d_time=0;
	        	        while (UartReady != SET && d_time !=60)
	        	         {
	        	        	HAL_Delay(1000);
	        	        	d_time++;
	        	         }
	        	        /* Reset transmission flag */
	        	        UartReady = RESET;
	        st_flags.sigfox = false;
	        find_between("RX=","\r",config,buff_downlink);
	        if(strlen(buff_downlink)>=15)
	        {
	            fn_fprint("SIGFOX DOWNLINK OK");
	            var=3;
	            st_flags.sigfox = true;
	            //fn_fprint(sigfox_downlink);
	            fn_fprint("SIGFOX DOWNLINK VALUES:");
	            fn_fprint(buff_downlink);
	            RemoveSpaces(buff_downlink);
	            hexBin (buff_downlink,buff_dec);

	            time_stamp[0] = buff_downlink[6];
	            time_stamp[1] = buff_downlink[7];
	            time_stamp[2] = buff_downlink[4];
	            time_stamp[3] = buff_downlink[5];
	            time_stamp[4] = buff_downlink[2];
	            time_stamp[5] = buff_downlink[3];


	            fn_fprint("SIGFOX DOWNLINK BINARY:");
	            fn_fprint(buff_dec);

	            //choice of transmission resource
	            if(buff_dec[4] == 49)
	                st_flags.en_sigfox = true;//buff_dec[2];
	            if(buff_dec[4] == 48)
	            	st_flags.en_sigfox = false;
	            if(buff_dec[3] == 49)
	            	st_flags.en_gprs = true;//buff_dec[2];
	            if(buff_dec[3] == 48)
	            	st_flags.en_gprs = false;

	            undTimeByte[0] = buff_dec[32];//0
	            undTimeByte[1] = buff_dec[33];//1
	            timebyte[0]    = buff_dec[34];//0
	            timebyte[1]    = buff_dec[35];//1
	            timebyte[2]    = buff_dec[36];//1
	            timebyte[3]    = buff_dec[37];//1
	            timebyte[4]    = buff_dec[38];//1
	            timebyte[5]    = buff_dec[39];//0

	            timebyte_int    =  atoi(timebyte);
	            undTimeByte_int =  atoi(undTimeByte);
	            st_data_sensor_e.Vol_ref = hexDec(volume_ref);

	            time_byte_value = binaryToDec(timebyte_int); //transmission period in hours

	            TimeStamp     = hexDec(time_stamp); //timestamp minutes-sigmais protocol

	            if(undTimeByte_int==00 && time_byte_value!=0) //if 00 = time unid its seconds
	            {st_timers.seconds_to_send = time_byte_value;}
	            if(undTimeByte_int==01 && time_byte_value!=0) // if 01 = time unid its minutes
	            {st_timers.seconds_to_send = (time_byte_value*60);}
	            if(undTimeByte_int==10 && time_byte_value!=0) // if 10 = time unid its hours
	            {st_timers.seconds_to_send = (time_byte_value*3600);}
	            if(undTimeByte_int==11 && time_byte_value!=0)// if 11 = time unid its days
	            {st_timers.seconds_to_send = (time_byte_value*86400);}

	            st_timers.seconds_today = fn_get_seconsForTimeStemp(TimeStamp); //seconds elapsed in the day

	            }
	    }

	     fn_uart_sleep();
}

void fn_send_config_frame()
{
}

void fn_send_start_frame()
{
	fn_reset_uart();
	fn_init_uart();
	char start_machine_frame[15]={0};
	//char payload_values[4]={0};
	char ok[4]={0};

	start_machine_frame[0]=65;
	start_machine_frame[1]=84;
	start_machine_frame[2]=36;
	start_machine_frame[3]=83;
	start_machine_frame[4]=70;
	start_machine_frame[5]=61;
	start_machine_frame[6]=48;
	start_machine_frame[7]=50;

	decHex(st_data_sensor_e.bat_value);
	size_2();
	strcat(start_machine_frame,buff);

	decHex(st_data_sensor_e.tpe_value);
	size_2();
	strcat(start_machine_frame,buff);

	start_machine_frame[12] = 13;
	start_machine_frame[13] = 10;
	start_machine_frame[14] = 0;
	HAL_UART_Transmit(&huart5,(uint8_t*)start_machine_frame,15,200);
	HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);

}
