/*
 * gsm.c
 *
 *  Created on: 2 de abr de 2019
 *      Author: Oscar Sigmais
 */

#include "gsm.h"
//#include <math.h>

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

//gsm varibles
uint8_t byte;
uint8_t byteGSM [BYTE_GSM_SIZE];
uint32_t bytesToReceive = sizeof(byteGSM);



/***************GSM FUNCTIONS*****************************************************/
int fn_check_ok()
{
	uint8_t ok = 0;
	for (int var = 0; var < strlen(nn_com); ++var)
	{
		if(nn_com[var] == 79 && nn_com[var+1] == 75)
		{
		ok = 1;
		break;
		}
	}

		return ok;
}

void fn_gsm_init()
{


	   fn_fprint("Manufacturer identification");
	   fn_send_gsm("AT+CGMI");
       fn_fprint(nn_com);
	   HAL_Delay(1000);
	   fn_fprint("Model identification");
	   fn_send_gsm("AT+CGMM");
       fn_fprint(nn_com);
	   HAL_Delay(1000);
	   fn_fprint("IMEI identification");
	   fn_send_gsm("AT+CGSN");
       fn_fprint(nn_com);
	   HAL_Delay(1000);
	   fn_fprint("SIM CARD identification");
	   fn_send_gsm("AT+CCID");
       fn_fprint(nn_com);
	   HAL_Delay(1000);
	   fn_fprint("CLOCK STATUS");
	   fn_send_gsm("AT+CCLK?");
       fn_fprint(nn_com);
	   HAL_Delay(1000);
}

void fn_gsm_start()
{
	if(id_uart[5]==0)
	{
		fn_wakeup_uart();
		fn_ID_UART();
		fn_uart_sleep();
	}
    HAL_Delay(500);
    st_flags.simcard = true;
    GSM_PWR_ON
    fn_fprint("GPRS PWR ON");
    HAL_Delay(1000);
    GSM_ON
    fn_fprint("GPRS ON ENABLE");
    HAL_Delay(30000);
    int count=10;
   while (fn_check_ok()==0 || count==0)
   {
   HAL_Delay(100);
   fn_send_gsm("AT&K0");
   fn_fprint(nn_com);
   count--;
   }
   fn_send_gsm("ATE0");
   HAL_Delay(100);
   fn_send_gsm("AT+CMEE=2");
   fn_fprint(nn_com);
   HAL_Delay(100);

   //fn_gsm_init();

   fn_send_gsm("AT+CPIN?");
   fn_fprint(nn_com);
   if(fn_check_ok()==0)
   {
	   st_flags.simcard  = false;
   }
   HAL_Delay(100);
   //fn_send_gsm("AT+CFUN=1");
  // HAL_Delay(500);
}

void fn_gsm_stop()
{
    GSM_PWR_OFF
    GSM_OFF
}

void fn_gsm_network_start()
{

/*        fn_send_gsm("AT+UPSD=1,1,\"tim.br\"");
        HAL_Delay(1000);
        fn_fprint(nn_com);
        fn_send_gsm("AT+UPSD=1,2,\"tim\"");
       HAL_Delay(1000);
       fn_fprint(nn_com);
        fn_send_gsm("AT+UPSD=1,3,\"tim\"");
       HAL_Delay(1000);
       fn_fprint(nn_com);*/
		fn_send_gsm("AT+CGATT=1");
		HAL_Delay(100);
		fn_fprint(nn_com);
		fn_send_gsm("AT+CREG=1");
		fn_fprint(nn_com);
        HAL_Delay(100);

        fn_send_gsm("AT+UPSDA=1,1");
        HAL_Delay(500);
        fn_fprint(nn_com);
        fn_send_gsm("AT+UPSDA=1,3");
        HAL_Delay(500);
        fn_fprint(nn_com);
        fn_send_gsm("AT+UPSD=1,8");
        HAL_Delay(500);
        fn_fprint(nn_com);
        fn_send_gsm("AT+UPSD=1,0");
        HAL_Delay(500);
        fn_fprint(nn_com);

        fn_send_gsm("AT+UPSND=1,0");//ip adress
        fn_fprint(nn_com);
        HAL_Delay(500);
/*		fn_send_gsm("AT+CREG?");
		fn_fprint(nn_com);
        HAL_Delay(500);
        fn_send_gsm("AT+COPS?");//network name
        fn_fprint(nn_com);
        HAL_Delay(500);
        fn_send_gsm("AT+CSQ");
        fn_fprint(nn_com);
        HAL_Delay(500);
        fn_send_gsm("AT+CGATT?");
        fn_fprint(nn_com);
        HAL_Delay(500);*/
}

void fn_gsm_downlink()
{
	fn_send_gsm("AT+UDELFILE=\"downlink.fss\"");
	fn_fprint(nn_com);
	HAL_Delay(1000);
    char payload_data[22]={0};
    hea_value=2;
    fn_mount_values(payload_data);
    char upload_data[80]="AT+UHTTPC=1,1,\"/gprs/bidir?ack=true&device=000000&data=";
	if(id_uart[1]!=48)
		{
		upload_data[43] = id_uart[0];
		upload_data[44] = id_uart[1];
		upload_data[45] = id_uart[2];
		upload_data[46] = id_uart[3];
		upload_data[47] = id_uart[4];
		upload_data[48] = id_uart[5];
		}
		if(id_uart[1]==48)
		{
			upload_data[43] = id_uart[2];
			upload_data[44] = id_uart[3];
			upload_data[45] = id_uart[4];
			upload_data[46] = id_uart[5];
			upload_data[47] = id_uart[6];
			upload_data[48] = id_uart[7];
		}
    fn_send_gsm("AT+UHTTP=1");
    fn_fprint(nn_com);
    HAL_Delay(1000);
    fn_send_gsm("AT+UHTTP=1,1,\"api.sigmais.com.br\"");
    fn_fprint(nn_com);
    HAL_Delay(1000);
    strcat(upload_data,payload_data);
    strcat(upload_data,"\",\"downlink.fss\"");
    strcat(upload_data,"\r\n\0");
    fn_send_gsm(upload_data);
    HAL_Delay(10000);
	fn_send_gsm("AT");
    HAL_Delay(1000);
	fn_send_gsm("AT+URDFILE=\"downlink.fss\"");
    HAL_Delay(5000);
    fn_fprint(nn_com);
    //fn_gprs_downlink_decoder(nn_com);

}

void fn_gsm_http_get()
{
    char payload_data[22]={0};
    hea_value=2;
    fn_mount_values(payload_data);
	fn_send_gsm("AT+UDELFILE=\"uplink.txt\"");
	fn_fprint(nn_com);
	HAL_Delay(1000);
    fn_send_gsm("AT+UHTTP=1");
    fn_fprint(nn_com);
    HAL_Delay(1000);
   fn_send_gsm("AT+UHTTP=1,1,\"api.sigmais.com.br\"");
   fn_fprint(nn_com);
    HAL_Delay(1000);
    char uhttpc[90]="AT+UHTTPC=1,0,\"/sigfox/uplink?device=";
    if(id_uart[1]!=48)
    {
    uhttpc[37] = id_uart[0];
    uhttpc[38] = id_uart[1];
    uhttpc[39] = id_uart[2];
    uhttpc[40] = id_uart[3];
    uhttpc[41] = id_uart[4];
    uhttpc[42] = id_uart[5];
    }
    if(id_uart[1]==48)
    {
        uhttpc[37] = id_uart[2];
        uhttpc[38] = id_uart[3];
        uhttpc[39] = id_uart[4];
        uhttpc[40] = id_uart[5];
        uhttpc[41] = id_uart[6];
        uhttpc[42] = id_uart[7];
    }
    //char* payload ="02241D3DE1417AFCA12345";
    //strcpy(uhttpc,"AT+UHTTPC=1,0,\"/sigfox/uplink?device=");
    strcat(uhttpc,"&data=");
    strcat(uhttpc,payload_data);
    strcat(uhttpc,"\",\"uplink.txt\"");
    strcat(uhttpc,"\r\n");
    //fn_send_gsm("AT+UHTTPC=1,0,\"/sigfox/uplink?device=0045C288&data=02241D3DE1417AFCA12345\",\"teste1.txt\"");
    fn_send_gsm(uhttpc);
    fn_fprint(nn_com);
    HAL_Delay(10000);
	fn_send_gsm("AT+URDFILE=\"uplink.txt\"");
    HAL_Delay(100);
    fn_fprint(nn_com);

}


void fn_send_gprs()
{
    fn_fprint("PREPARE TO SEND BY GPRS");
    fn_gsm_start();
    if(st_flags.simcard  == true)
    {
		fn_fprint("GPRS NETWORK CONFIGURATIONS");
		fn_gsm_network_start();
		fn_fprint("GPRS SENDING RAW DATA");
		fn_gsm_http_get_raw();
		fn_fprint("GPRS SENDING PAYLOAD DATA");
		fn_gsm_http_get();
    }
    fn_fprint("GPRS TURN OFF");
    fn_gsm_stop();
}

void fn_send_gprs_downlink()
{
    fn_fprint("PREPARE TO SEND BY GPRS");
    fn_gsm_start();
    if(st_flags.simcard  == true)
    {
		fn_fprint("GPRS NETWORK CONFIGURATIONS");
		fn_gsm_network_start();
		fn_fprint("GPRS SEND DOWNLINK");
		fn_gsm_downlink();
    }
    fn_fprint("GPRS TURN OFF");
    fn_gsm_stop();
}


void clearBuffer(uint8_t *pbuffer,  uint8_t bufferSize){

    for (uint8_t ii = 0; ii < bufferSize; ii++ ){
        pbuffer[ii] = 0;
    }
}

void fn_send_gsm(char* command)
{
	//HAL_UART_Transmit(&huart2,(uint8_t*)"AT&K0/r/n/0",11,100);
	memset(nn_com,0,500);
    int delay_gsm=3000;
    int tamanho = strlen(command);
    tamanho+=3;
    char new_command[tamanho];
    strcpy(new_command,command);
    strcat(new_command,"\r\n\0");
    uint8_t* new_com = (uint8_t*)new_command;

    uint8_t estado = 0;
    uint8_t posFimString = 0;

    if(HAL_UART_Transmit_IT(&huart2,(uint8_t*)new_com,tamanho)==HAL_OK)

    {
    	HAL_UART_Transmit_IT(&huart1, (uint8_t*)new_com, tamanho);
        estado = 1;
        HAL_Delay(300);
    }

    //HAL_UART_Receive(&huart1, &byte, 1,200);
    HAL_UART_Receive_IT(&huart2, (uint8_t*)&byteGSM, bytesToReceive);

	if(strcmp(command,"AT+UPSDA=1,3")==0)//strcmp(command,"AT+URDFILE=\"resposta.txt\"")==0 ||
	{
		delay_gsm=20000;

	}
  if(estado== 1)
      {
          HAL_Delay(delay_gsm);
          huart2.RxXferCount = 0;
          estado = 0;
          byte = 0;
          huart2.pRxBuffPtr =(uint8_t*) &byteGSM;
          for (uint16_t ii = 0; ii <bytesToReceive; ii++ ){
              if (byteGSM[ii] == 0)
                  break;

              //(uint8_t*) nn_com[ii] = byteGSM[ii];
              posFimString++;
          }
          strncpy(nn_com,(char*)byteGSM,500);
          //HAL_UART_Transmit(&huart1,nn_com[ii], posFimString,1);
          //if(strcmp (nn_com,"at+cpin?\r\r\n+CPIN: READY\r\n\r\nOK\r\n")==0)
         // {
          st_flags.simcard  = true;
         // }
          HAL_Delay(delay_gsm);
          clearBuffer(byteGSM,posFimString);
      }

 //
}

int fn_get_gsm_response()
{
    int response_size = strlen(nn_com);
    for (int var = 0; var < response_size; var++)
    {
        if(nn_com[var]=='\0')
        {
            break;
        }
        if(nn_com[var]==79 && nn_com[var+1]==75)
        {
            return 1;
        }

    }
   return 0;
}



