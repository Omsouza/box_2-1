/*
 * gps.c
 *
 *  Created on: 2 de abr de 2019
 *      Author: Oscar Sigmais
 */


#include "gps.h"



UART_HandleTypeDef huart4;




//extern unsigned int hora,lat_value,lon_value;



void fn_get_gps()
{
	//***************GPS VARIABLES**********************************
	float data_latitude,data_longitude;
    char buffer_gps[1024]="";
    char aux_buff_gps[74]="";
    char lat    [12]="";
    char lon    [12]="";
    char hra    [10]="";

	GPS_ON


    HAL_UART_Receive(&huart4,(uint8_t*)buffer_gps,1024,2000);


    //$xxGGA,time,lat,NS,long,EW,quality,numSV,HDOP,alt,M,sep,M,diffAge,diffStation*cs<CR><LF>
    find_between("$GNGGA,", "*",buffer_gps, aux_buff_gps);

/*    if(strlen(aux_buff_gps)<55)
    {
        fn_fprint("GPS NO FOUND");
    }*/
    if(strlen(aux_buff_gps)>55)
    {
        int c =0;
        unsigned int hour, minute, second;
        int lat_graus,lon_graus;
        float lat_min,lon_min;
        char* token;
        token = strtok(aux_buff_gps, ",");
        while (token != NULL)
            {
            if (c==0)
                strcpy(hra,token);
            if (c==1)
                strcpy(lat,token);
            if (c==2)
            {}
            if (c==3)
                strcpy(lon,token);
            if (c==4)
            {}
            if (c==5)
            {}
            if (c==6)
            {}
            if (c==7)
            {}
            if (c==8)
            {}
            if (c==9)
                token = NULL;
            c++;
            token = strtok(NULL, ",");
            }

            hora = atoi(hra);              // hora (hh.mm.ss)
            data_latitude = atof(lat);      // latitude
            data_longitude = atof(lon);     // longitude
            hour = hora/10000;
            minute = (hora-(hour*10000))/100;
            second = (hora-((hour*10000)+(minute*100)));
            if(hora!=0)
            {
                switch (hour)
                {
                    case 2:
                        hour = 23;
                        break;
                    case 1:
                        hour = 22;
                        break;
                    case 0:
                        hour = 21;
                        break;
                    default:
                        hour-=3;
                        break;
                }
            }
            if((data_latitude != 0) && (data_longitude  != 0))
            {
            	st_flags.gps = true;
                data_latitude/=100;
                data_longitude/=100;
                lat_graus = (int) data_latitude;
                lon_graus = (int) data_longitude;
                lat_min = (data_latitude-lat_graus)*100;
                lon_min = (data_longitude-lon_graus)*100;
                data_latitude = lat_graus + (lat_min/60);
                data_longitude = lon_graus + (lon_min/60);
                st_data_sensor_e.lat_value = data_latitude*100000;
                st_data_sensor_e.log_value = data_longitude*100000;

                st_data_sensor_previwes_e.lat_value = st_data_sensor_e.lat_value;
                st_data_sensor_previwes_e.log_value = st_data_sensor_e.log_value;
                GPS_OFF
            }
            if(sigfox_downlink == false)
            {
                st_timers.seconds_today = second + (minute*60) + (hour*3600);
            }

    }
    if(st_data_sensor_e.lat_value == st_data_sensor_previwes_e.lat_value &&
    		st_data_sensor_e.log_value == st_data_sensor_previwes_e.log_value)

        {
    	st_data_sensor_previwes_e.lat_value = 0;
    	st_data_sensor_previwes_e.log_value = 0;

        }


}

void fn_init_gps()
{
	GPS_ON
	HAL_Delay(10000);
	st_flags.gps = false;
	st_data_sensor_e.lat_value = 0;
	st_data_sensor_e.log_value = 0;
	st_data_sensor_previwes_e.log_value = 0;
	st_data_sensor_previwes_e.lat_value = 0;
    fn_get_gps();
}



