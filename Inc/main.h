/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <lsm303agr_reg.h>
#include <sigfox.h>
#include <sensors.h>
#include <encoder.h>
#include <gps.h>
#include <gsm.h>


/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
int TimeStamp;
__IO ITStatus UartReady;// = RESET;
//*****************TIME VARIABLES*********************************
//unsigned int Time_Machine, cont_time;

struct
{
	unsigned int
	Time_Machine,
    cont_time,
    seconds_today,
	seconds_to_send;
}st_timers;

struct
{
	bool
	simcard,
	interrupt,
	gps,
	gprs,
	sigfox,
	en_gprs,
	en_sigfox,
	config;
}st_flags;
enum
{
	DATA_REPORT_FRAME,
	DATA_ALERT_FRAME,
	DATA_START_FRAME,
	CONFIG_REPORT_FRAME,
	CONFIG_REQUEST_FRAME
} e_uplink_frames_type_t;

enum
{
	DAILY_UPDATE_FRAME,
	CONFIG_FRAME
} e_downlink_frame_type_t;



typedef struct
{
	//e_uplink_frames_type_t header;
	uint32_t agl_value;
	uint32_t bat_value;
	int32_t vol_value;
	uint32_t hum_value;
	uint32_t tpe_value;
	uint32_t lat_value;
	uint32_t log_value;
	uint32_t Vol_ref;

}st_sensors_data_t;

st_sensors_data_t st_data_sensor_e;
st_sensors_data_t st_data_sensor_previwes_e;


/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */


/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

void fn_get_adc_channels();
/*void payload_init();*/

void blink();

void fn_init();

void fn_plot_results();

void fn_send_payload();

void fn_fprint(char *data);

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define VCC_GPS_EN_Pin GPIO_PIN_0
#define VCC_GPS_EN_GPIO_Port GPIOC
#define VCC_SEN_EN_Pin GPIO_PIN_1
#define VCC_SEN_EN_GPIO_Port GPIOC
#define US2_AN_Pin GPIO_PIN_3
#define US2_AN_GPIO_Port GPIOC
#define VBAT_LEVEL_Pin GPIO_PIN_0
#define VBAT_LEVEL_GPIO_Port GPIOA
#define GSM_PWR_ON_Pin GPIO_PIN_5
#define GSM_PWR_ON_GPIO_Port GPIOA
#define WS_RST_Pin GPIO_PIN_2
#define WS_RST_GPIO_Port GPIOB
#define TEMP_I2C_SCL_Pin GPIO_PIN_10
#define TEMP_I2C_SCL_GPIO_Port GPIOB
#define TEMP_I2C_SDA_Pin GPIO_PIN_11
#define TEMP_I2C_SDA_GPIO_Port GPIOB
#define VCC_GSM_EN_Pin GPIO_PIN_7
#define VCC_GSM_EN_GPIO_Port GPIOC
#define LED_Pin GPIO_PIN_2
#define LED_GPIO_Port GPIOD
#define WS_RX_Pin GPIO_PIN_3
#define WS_RX_GPIO_Port GPIOB
#define WS_TX_Pin GPIO_PIN_4
#define WS_TX_GPIO_Port GPIOB
#define WS_WAKEUP_Pin GPIO_PIN_5
#define WS_WAKEUP_GPIO_Port GPIOB
#define LSM_I2C_SCL_Pin GPIO_PIN_6
#define LSM_I2C_SCL_GPIO_Port GPIOB
#define LSM_I2C_SDA_Pin GPIO_PIN_7
#define LSM_I2C_SDA_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#define LED_ON              HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin,1);
#define LED_OFF             HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin,0);
#define LED_CHANGE          HAL_GPIO_TogglePin(LED_GPIO_Port,LED_Pin);

#define GSM_ON              HAL_GPIO_WritePin(VCC_GSM_EN_GPIO_Port,VCC_GSM_EN_Pin,1);
#define GSM_OFF             HAL_GPIO_WritePin(VCC_GSM_EN_GPIO_Port,VCC_GSM_EN_Pin,0);
#define GSM_PWR_ON          HAL_GPIO_WritePin(GSM_PWR_ON_GPIO_Port,GSM_PWR_ON_Pin,1);
#define GSM_PWR_OFF         HAL_GPIO_WritePin(GSM_PWR_ON_GPIO_Port,GSM_PWR_ON_Pin,0);
#define SEN_ON              HAL_GPIO_WritePin(VCC_SEN_EN_GPIO_Port,VCC_SEN_EN_Pin,1);
#define SEN_OFF             HAL_GPIO_WritePin(VCC_SEN_EN_GPIO_Port,VCC_SEN_EN_Pin,0);
#define GPS_ON              HAL_GPIO_WritePin(VCC_GPS_EN_GPIO_Port,VCC_GPS_EN_Pin,1);
#define GPS_OFF             HAL_GPIO_WritePin(VCC_GPS_EN_GPIO_Port,VCC_GPS_EN_Pin,0);




/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
