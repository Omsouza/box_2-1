/*
 * sensors.h
 *
 *  Created on: 1 de ago de 2019
 *      Author: oscar
 */

#ifndef SENSORS_H_
#define SENSORS_H_


#include "main.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <sigfox.h>
#include <lsm303agr_reg.h>
//******************** PAYLOAD VALUES*****************************
//unsigned int bat_value,ph_value,vol_value, hum_value, tpe_value, tpl_value;

//**************VOLUME VARIABLES*********************************
unsigned int us2_value;
uint32_t us2_volt,us2_raw;
//**************BATTERY VARIABLES*********************************
float bat_volt,bat_raw,v_ref,vdd,bat_percent;
//**************SI7021 VARIABLES*********************************
unsigned int rawT, rawH;
float Temperature, Humidity;
//**************LSM303 VARIABLES*********************************
float temperature_degC;
int16_t a_x, a_y, a_z;
int16_t m_x, m_y, m_z;
int16_t pitch_y, pitch, roll, roll_x, yaw, yaw_y;
//**************STM32 VARIBLES************************************
uint32_t Vref, STemp,Vref_raw, STemp_raw;

void fn_read_sensors();

void fn_get_temp();

void fn_get_volume();

void fn_get_battery();

void fn_get_si7021();

void fn_init_lsm();

void fn_get_lsm();

void fn_get_ph();

void fn_get_MLX90640();

 int32_t platform_write(void *handle, uint8_t Reg, uint8_t *Bufp,
                              uint16_t len);
 int32_t platform_read(void *handle, uint8_t Reg, uint8_t *Bufp,
                            uint16_t len);
#endif /* SENSORS_H_ */
