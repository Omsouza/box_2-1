/*
 * gps.h
 *
 *  Created on: 2 de abr de 2019
 *      Author: Oscar Sigmais
 */

#ifndef GPS_H_
#define GPS_H_

#ifdef __cplusplus
  extern "C" {
#endif

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "main.h"

#include <sigfox.h>

unsigned int hora;


void find_between(const char *first, const char *last,char *buff,char *buff_return);

void fn_init_gps();

void fn_get_gps();

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* GPS_H_ */
