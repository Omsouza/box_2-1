/*
 * sigfox.h
 *
 *  Created on: 1 de ago de 2019
 *      Author: oscar
 */

#ifndef SIGFOX_H_
#define SIGFOX_H_

#include <main.h>

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <lsm303agr_reg.h>
#include <sensors.h>
#include <encoder.h>
#include <gps.h>
#include <gsm.h>

#define DONGLE_KEY
#define sec_to_1_min		60
#define sec_to_2_min		120
#define sec_to_5_min		300
#define sec_to_10_min		600
#define sec_to_15_min		900
#define sec_to_25_min		1500
#define sec_to_30_min		1800
#define sec_to_1_hour		3600
#define sec_to_2_hour		7200
#define sec_to_6_hour		21600
#define sec_to_8_hour		28600
#define sec_to_12_hour	    43200
#define sec_to_24_hour	    86400

void fn_init_uart();
void fn_ID_UART();
void fn_PAC_UART();
void fn_at_uart();
void fn_uart_sleep();
void fn_reset_uart();
void fn_wakeup_uart();
void fn_temp_UART();
void fn_volts_UART();
void fn_send_report_frame();
void fn_send_daily_frame();
void fn_send_config_frame();
void fn_send_start_frame();
void fn_send_alert_frame();

UART_HandleTypeDef huart5;
UART_HandleTypeDef huart1;

//****************SIGFOX VARIABLES******************************
char id_uart[10];
char pac_uart[18];
char at_uart[4];
float uart_temp, uart_volt;
int id_sigfox_int, pac_sigfox_int;
bool sigfox_downlink;



#endif /* SIGFOX_H_ */
