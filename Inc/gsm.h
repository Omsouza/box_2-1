/*
 * gsm.h
 *
 *  Created on: 2 de abr de 2019
 *      Author: Oscar Sigmais
 */

#ifndef GSM_H_
#define GSM_H_

#include "main.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <encoder.h>
#include <sigfox.h>
#include <gps.h>


#define GSM_RESET_ON        HAL_GPIO_WritePin(GSM_RESET_N_GPIO_Port,GSM_RESET_N_Pin,0);
#define GSM_RESET_OFF       HAL_GPIO_WritePin(GSM_RESET_N_GPIO_Port,GSM_RESET_N_Pin,1);
#define GSM_ON              HAL_GPIO_WritePin(VCC_GSM_EN_GPIO_Port,VCC_GSM_EN_Pin,1);
#define GSM_OFF             HAL_GPIO_WritePin(VCC_GSM_EN_GPIO_Port,VCC_GSM_EN_Pin,0);
#define GSM_PWR_ON          HAL_GPIO_WritePin(GSM_PWR_ON_GPIO_Port,GSM_PWR_ON_Pin,1);
#define GSM_PWR_OFF         HAL_GPIO_WritePin(GSM_PWR_ON_GPIO_Port,GSM_PWR_ON_Pin,0);


#define BYTE_GSM_SIZE       500


char nn_com[BYTE_GSM_SIZE];
/* Single byte to store input */


void fn_send_gsm(char* command);

int fn_get_gsm_response();

void clearBuffer(uint8_t *pbuffer,  uint8_t bufferSize);


void fn_get_test();

void fn_gsm_http_get();

void fn_gsm_network_start();

void fn_gsm_stop();

void fn_gsm_start();

void fn_send_gprs();

void fn_gsm_init();

void fn_gsm_downlink();

//void clearBuffer(uint8_t *pbuffer,  uint8_t bufferSize);

void fn_send_gprs();

void fn_send_gprs_downlink();

void fn_gsm_http_get_raw();

int fn_check_ok();

#endif /* GSM_H_ */
